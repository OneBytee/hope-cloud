package com.lh.hope.cloud.gateway.controller;

import com.lh.hope.cloud.common.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/29 17:41
 * @version: 1.0
 * @description:
 */
@RestController
public class FallbackController {

    @GetMapping("/fallback")
    public Result fallback() {
        return new Result(null, "Get request fallback!", 500);
    }
}
