package com.lh.hope.cloud.system.controller;

import com.lh.hope.cloud.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/28 14:53
 * @version: 1.0
 * @description:
 */
@Slf4j
@RestController
public class SystemController {

    @Value("${spring.application.name}")
    private String springApplicationName;

    @Value("${server.port}")
    private Integer serverPort;

    @GetMapping(value = "/queryPort")
    public Result<String> queryPort(String appName, Integer appPort) {
        try {
           Thread.sleep(2000L);
        } catch (Exception e){
        }
        String s = "consumer[{%s}:{%s}] provider[{%s}:{%s}]";
        s = String.format(s, appName, appPort, springApplicationName, serverPort);
        log.info("consumer[{}:{}] provider[{}:{}]", appName, appPort, springApplicationName, serverPort);
        return Result.ok(s);
    }
}
