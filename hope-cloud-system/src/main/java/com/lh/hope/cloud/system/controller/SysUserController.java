package com.lh.hope.cloud.system.controller;

import com.lh.hope.cloud.common.Result;
import com.lh.hope.cloud.common.entity.SysUser;
import com.lh.hope.cloud.system.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/28 16:10
 * @version: 1.0
 * @description:
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @PostMapping("/insert")
    public Result insert(@RequestBody SysUser user) {
        sysUserService.insert(user);
        return new Result("操作成功", 200);
    }

    @GetMapping("/{id}")
    public Result<SysUser> getUser(@PathVariable Long id) {
        SysUser user = sysUserService.getUser(id);
        log.info("根据id获取用户信息，用户名称为：{}", user.getUsername());
        return new Result<SysUser>(user);
    }

    @GetMapping("/listUsersByIds")
    public Result<List<SysUser>> listUsersByIds(@RequestParam List<Long> ids) {
        List<SysUser> userList = sysUserService.listUsersByIds(ids);
        log.info("根据ids获取用户信息，用户列表为：{}", userList);
        return new Result<List<SysUser>>(userList);
    }

    @GetMapping("/getByUsername")
    public Result<SysUser> getByUsername(@RequestParam String username) {
        SysUser user = sysUserService.getByUsername(username);
        return new Result<SysUser>(user);
    }

    @PostMapping("/update")
    public Result update(@RequestBody SysUser user) {
        sysUserService.update(user);
        return new Result("操作成功", 200);
    }

    @PostMapping("/delete/{id}")
    public Result delete(@PathVariable Long id) {
        sysUserService.delete(id);
        return new Result("操作成功", 200);
    }
}
