package com.lh.hope.cloud.system.service;

import com.lh.hope.cloud.common.entity.SysUser;

import java.util.List;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/28 16:11
 * @version: 1.0
 * @description:
 */
public interface SysUserService {

    void insert(SysUser user);

    SysUser getUser(Long id);

    void update(SysUser user);

    void delete(Long id);

    SysUser getByUsername(String username);

    List<SysUser> listUsersByIds(List<Long> ids);
}
