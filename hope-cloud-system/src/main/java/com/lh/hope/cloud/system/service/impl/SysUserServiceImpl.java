package com.lh.hope.cloud.system.service.impl;

import com.lh.hope.cloud.common.entity.SysUser;
import com.lh.hope.cloud.system.service.SysUserService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/28 16:15
 * @version: 1.0
 * @description:
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    private List<SysUser> userList;

    @PostConstruct
    public void initData() {
        userList = new ArrayList<>();
        userList.add(new SysUser(1L, "jourwon", "123456"));
        userList.add(new SysUser(2L, "andy", "123456"));
        userList.add(new SysUser(3L, "mark", "123456"));
    }

    @Override
    public void insert(SysUser user) {
        userList.add(user);
    }

    @Override
    public SysUser getUser(Long id) {
        List<SysUser> list = userList.stream().filter(userItem -> userItem.getId().equals(id)).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public void update(SysUser user) {
        userList.stream().filter(userItem -> userItem.getId().equals(user.getId())).forEach(userItem -> {
            userItem.setUsername(user.getUsername());
            userItem.setPassword(user.getPassword());
        });
    }

    @Override
    public void delete(Long id) {
        SysUser user = getUser(id);
        if (user != null) {
            userList.remove(user);
        }
    }

    @Override
    public SysUser getByUsername(String username) {
        List<SysUser> list = userList.stream().filter(userItem -> userItem.getUsername().equals(username)).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<SysUser> listUsersByIds(List<Long> ids) {
        return userList.stream().filter(userItem -> ids.contains(userItem.getId())).collect(Collectors.toList());
    }
}
