package com.lh.hope.cloud.system.controller;

import com.lh.hope.cloud.common.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/28 15:13
 * @version: 1.0
 * @description:
 */
@RestController
@RequestMapping("/ribbon")
public class RibbonController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${spring.application.name}")
    private String springApplicationName;

    @Value("${server.port}")
    private Integer serverPort;

    @Value("${service-url.system-service}")
    private String systemServiceUrl;

    @GetMapping("/queryPort")
    public Result queryPort() {
        return restTemplate.getForObject(systemServiceUrl + "/queryPort/?appName=" + springApplicationName + "&appPort=" + serverPort, Result.class);
    }
}
