package com.lh.hope.cloud.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/28 14:39
 * @version: 1.0
 * @description:
 */
@EnableFeignClients
@EnableCircuitBreaker
@EnableDiscoveryClient
@SpringBootApplication
public class MgtApplication {
    public static void main(String[] args) {
        SpringApplication.run(MgtApplication.class, args);
    }

}
