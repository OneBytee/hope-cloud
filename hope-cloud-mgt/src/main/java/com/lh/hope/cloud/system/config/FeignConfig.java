package com.lh.hope.cloud.system.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/29 14:11
 * @version: 1.0
 * @description:
 */
@Configuration
public class FeignConfig {

    @Bean
    Logger.Level level() {
        return Logger.Level.FULL;
    }
}
