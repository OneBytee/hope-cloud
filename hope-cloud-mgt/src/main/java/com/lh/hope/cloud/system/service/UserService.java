package com.lh.hope.cloud.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.lh.hope.cloud.common.Result;
import com.lh.hope.cloud.common.entity.SysUser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheRemove;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import com.netflix.hystrix.contrib.javanica.command.AsyncResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/28 16:38
 * @version: 1.0
 * @description:
 */
@Slf4j
@Service
public class UserService {
    /**
     * @HystrixCommand详解
     * @HystrixCommand 中的常用参数 fallbackMethod：指定服务降级处理方法；
     * - ignoreExceptions：忽略某些异常，不发生服务降级；
     * - commandKey：命令名称，用于区分不同的命令；
     * - groupKey：分组名称，Hystrix会根据不同的分组来统计命令的告警及仪表盘信息；
     * - threadPoolKey：线程池名称，用于划分线程池。
     * ————————————————
     */
    @Autowired
    private RestTemplate restTemplate;

    @Value("${service-url.system-service}")
    private String userServiceUrl;

    /**
     * fallbackMethod：指定服务降级处理方法；
     *
     * @param id
     * @return
     */
    @HystrixCommand(fallbackMethod = "fallbackMethod1")
    public Result getUser(Long id) {
        return restTemplate.getForObject(userServiceUrl + "/user/{1}", Result.class, id);
    }

    /**
     * 声明的参数需要包含controller的声明参数
     *
     * @param id
     * @return
     */
    public Result fallbackMethod1(@PathVariable Long id) {
        log.info("fallback->id={}", id);
        return new Result("调用system服务失败", 500);
    }

    /**
     * ignoreExceptions：忽略某些异常，不发生服务降级；
     * <p>
     * 空指针异常时不降级
     *
     * @param id
     * @return
     */
    @HystrixCommand(fallbackMethod = "fallbackMethod2", ignoreExceptions = {NullPointerException.class})
    public Result getUserException(Long id) {
        if (id == 1) {
            throw new IndexOutOfBoundsException();
        } else if (id == 2) {
            throw new NullPointerException();
        }
        return restTemplate.getForObject(userServiceUrl + "/user/{1}", Result.class, id);
    }

    public Result fallbackMethod2(@PathVariable Long id, Throwable e) {
        log.error("id {},throwable class:{}", id, e.getClass());
        return new Result("服务调用失败", 500);
    }

    @HystrixCommand(fallbackMethod = "fallbackMethod1",
            commandKey = "getUserCommand",
            groupKey = "getUserGroup",
            threadPoolKey = "getUserThreadPool")
    public Result getUserCommand(Long id) {
        return restTemplate.getForObject(userServiceUrl + "/user/{1}", Result.class, id);
    }

    /**
     * 当系统并发量越来越大时，我们需要使用缓存来优化系统，达到减轻并发请求线程数，提供响应速度的效果。
     *
     * @param id
     * @return
     */
    @CacheResult(cacheKeyMethod = "getCacheKey")
    @HystrixCommand(fallbackMethod = "fallbackMethod1", commandKey = "getUserCache")
    public Result getUserCache(Long id) {
        log.info("getUserCache id:{}", id);
        return restTemplate.getForObject(userServiceUrl + "/user/{1}", Result.class, id);
    }

    /**
     * 为缓存生成key的方法
     *
     * @return
     */
    public String getCacheKey(Long id) {
        return String.valueOf(id);
    }

    @HystrixCommand
    @CacheRemove(commandKey = "getUserCache", cacheKeyMethod = "getCacheKey")
    public Result removeCache(Long id) {
        log.info("removeCache id:{}", id);
        return restTemplate.postForObject(userServiceUrl + "/user/delete/{1}", null, Result.class, id);
    }

    @HystrixCollapser(batchMethod = "listUsersByIds", collapserProperties = {
            @HystrixProperty(name = "timerDelayInMilliseconds", value = "100")
    })
    public Future<SysUser> getUserFuture(Long id) {
        return new AsyncResult<SysUser>() {
            @Override
            public SysUser invoke() {
                Result result = restTemplate.getForObject(userServiceUrl + "/user/{1}", Result.class, id);
                Map data = (Map) result.getData();
                SysUser user = BeanUtil.mapToBean(data, SysUser.class, true);
                log.info("getUserById username:{}", user.getUsername());
                return user;
            }
        };
    }

    @HystrixCommand
    public List<SysUser> listUsersByIds(List<Long> ids) {
        log.info("listUsersByIds:{}", ids);
        Result result = restTemplate.getForObject(userServiceUrl + "/user/listUsersByIds?ids={1}", Result.class, CollUtil.join(ids, ","));
        return (List<SysUser>) result.getData();
    }
}
