package com.lh.hope.cloud.system.service;

import com.lh.hope.cloud.common.Result;
import com.lh.hope.cloud.common.entity.SysUser;
import com.lh.hope.cloud.system.service.fallback.UserFeignFallbackService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/29 11:29
 * @version: 1.0
 * @description:
 */
@FeignClient(value = "hope-cloud-system", fallback = UserFeignFallbackService.class)
public interface UserFeignService {

    @PostMapping("/user/insert")
    Result insert(@RequestBody SysUser user);

    @GetMapping("/user/{id}")
    Result<SysUser> getUser(@PathVariable Long id);

    @GetMapping("/user/listUsersByIds")
    Result<List<SysUser>> listUsersByIds(@RequestParam List<Long> ids);

    @GetMapping("/user/getByUsername")
    Result<SysUser> getByUsername(@RequestParam String username);

    @PostMapping("/user/update")
    Result update(@RequestBody SysUser user);

    @PostMapping("/user/delete/{id}")
    Result delete(@PathVariable Long id);

}
