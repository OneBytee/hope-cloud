package com.lh.hope.cloud.system.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONObject;
import com.lh.hope.cloud.common.entity.UserDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/30 17:27
 * @version: 1.0
 * @description:
 */
@RestController
@RequestMapping("/user/auth")
public class AuthController {
    @GetMapping("/currentUser")
    public UserDTO currentUser(@RequestHeader("user") String user) {
        JSONObject userJsonObject = new JSONObject(user);
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(userJsonObject.getStr("user_name"));
        userDTO.setId(Convert.toLong(userJsonObject.get("id")));
        userDTO.setRoles(Convert.toList(String.class, userJsonObject.get("authorities")));
        return userDTO;
    }

}
