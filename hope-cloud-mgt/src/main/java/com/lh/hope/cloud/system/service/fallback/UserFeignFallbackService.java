package com.lh.hope.cloud.system.service.fallback;

import com.lh.hope.cloud.common.Result;
import com.lh.hope.cloud.common.entity.SysUser;
import com.lh.hope.cloud.system.service.UserFeignService;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/29 11:30
 * @version: 1.0
 * @description:
 */
@Component
public class UserFeignFallbackService implements UserFeignService {
    @Override
    public Result insert(SysUser user) {
        return new Result("调用失败，服务被降级", 500);
    }

    @Override
    public Result<SysUser> getUser(Long id) {
        return new Result("调用失败，服务被降级", 500);
    }

    @Override
    public Result<List<SysUser>> listUsersByIds(List<Long> ids) {
        return new Result("调用失败，服务被降级", 500);
    }

    @Override
    public Result<SysUser> getByUsername(String username) {
        return new Result("调用失败，服务被降级", 500);
    }

    @Override
    public Result update(SysUser user) {
        return new Result("调用失败，服务被降级", 500);
    }

    @Override
    public Result delete(Long id) {
        return new Result("调用失败，服务被降级", 500);
    }
}
