package com.lh.hope.cloud.system.controller;

import com.lh.hope.cloud.common.Result;
import com.lh.hope.cloud.common.entity.SysUser;
import com.lh.hope.cloud.system.service.UserFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/29 13:49
 * @version: 1.0
 * @description:
 */
@RestController
@RequestMapping("/user/feign")
public class UserFeignController {

    @Autowired
    private UserFeignService userFeignService;

    @PostMapping("/insert")
    public Result insert(@RequestBody SysUser user) {
        return userFeignService.insert(user);
    }

    @GetMapping("/{id}")
    public Result<SysUser> getUser(@PathVariable Long id) {
        return userFeignService.getUser(id);
    }

    @GetMapping("/listUsersByIds")
    public Result<List<SysUser>> listUsersByIds(@RequestParam List<Long> ids) {
        return userFeignService.listUsersByIds(ids);
    }

    @GetMapping("/getByUsername")
    public Result<SysUser> getByUsername(@RequestParam String username) {
        return userFeignService.getByUsername(username);
    }

    @PostMapping("/update")
    public Result update(@RequestBody SysUser user) {
        return userFeignService.update(user);
    }

    @PostMapping("/delete/{id}")
    public Result delete(@PathVariable Long id) {
        return userFeignService.delete(id);
    }

}
