package com.lh.hope.cloud.system.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/30 17:40
 * @version: 1.0
 * @description:
 */
@RestController
@RequestMapping("/mgt")
public class MgtController {

    @GetMapping("/hello")
    private String hello() {
        return "hello mgt~";
    }
}
