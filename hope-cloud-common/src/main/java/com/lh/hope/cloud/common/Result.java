package com.lh.hope.cloud.common;


import lombok.Data;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/28 14:46
 * @version: 1.0
 * @description:
 */
@Data
public class Result<T> {
    private T data;

    private String message;

    private int code;

    public Result() {
    }

    public Result(T data, String message, int code) {
        this.data = data;
        this.message = message;
        this.code = code;
    }

    public Result(String message, Integer code) {
        this(null, message, code);
    }

    public Result(T data) {
        this(data, "操作成功", 200);
    }

    public static <T> Result<T> ok(T data) {
        return new Result<T>(data);
    }

    public static <T> Result<T> fail(Integer code, String message, T data) {
        return new Result<>(data, message, code);
    }

}
