package com.lh.hope.cloud.auth.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/30 11:18
 * @version: 1.0
 * @description:
 */

public interface UserService extends UserDetailsService {
}
