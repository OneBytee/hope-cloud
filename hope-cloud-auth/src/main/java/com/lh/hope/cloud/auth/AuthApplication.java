package com.lh.hope.cloud.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/30 11:15
 * @version: 1.0
 * @description:
 */
@SpringBootApplication
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }
}
