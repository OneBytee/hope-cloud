package com.lh.hope.cloud.auth.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.lh.hope.cloud.auth.service.ResourceService;
import com.lh.hope.cloud.common.constant.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author: liuhang@lakala.com
 * @createDate: 2021/7/30 16:45
 * @version: 1.0
 * @description:
 */
@Service
public class ResourceServiceImpl implements ResourceService {

    private Map<String, List<String>> resourceRolesMap;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @PostConstruct
    public void initData() {
        resourceRolesMap = new TreeMap<>();
        resourceRolesMap.put("/mgt/mgt/hello", CollUtil.toList("ADMIN"));
        resourceRolesMap.put("/mgt/user/auth/currentUser", CollUtil.toList("ADMIN", "TEST"));
        redisTemplate.opsForHash().putAll(RedisConstant.RESOURCE_ROLES_MAP, resourceRolesMap);
    }
}
